# Install Pgweb in Debian Linux

This repository is dedicated to the installation of Pgweb in a Debian-based host OS.  Pgweb is used for viewing PostgreSQL databases.

## What's wrong with pgAdmin 3?
It's obsolete and does not support any version of PostgreSQL beyond version 9.

## What's wrong with pgAdmin 4?
It's much more difficult to install than pgAdmin 3.

## Why do you like Pgweb?
It's free, open source, and easy to install and use.
