#!/bin/bash

echo '-------------------'
echo 'sudo apt-get update'
sudo apt-get update

echo '----------------------------------'
echo 'sudo apt-get install -y curl unzip'
sudo apt-get install -y curl unzip

echo '-----------------'
echo 'Downloading Pgweb'
curl -s https://api.github.com/repos/sosedoff/pgweb/releases/latest \
  | grep linux_amd64.zip \
  | grep download \
  | cut -d '"' -f 4 \
  | wget -qi - \

wait

echo '---------------------------'
echo 'unzip pgweb_linux_amd64.zip'
unzip pgweb_linux_amd64.zip

wait

echo '------------------------'
echo 'rm pgweb_linux_amd64.zip'
rm pgweb_linux_amd64.zip

echo '----------------------------------------------'
echo 'sudo mv pgweb_linux_amd64 /usr/local/bin/pgweb'
sudo mv pgweb_linux_amd64 /usr/local/bin/pgweb
